package cz.filtry.database;

import cz.filtry.model.Plechy;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PlechyDatabase {
    private Connection connection;

    public PlechyDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addPlech(String nazev, int kusu, int tloustka, String tvarderovani, int vyska, int sirka, String material) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("INSERT into Plechy (nazev,kusu,tloustka,tvarderovani,vyska,sirka,material) values(?,?,?,?,?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setInt(2, kusu);
            preparedStatement.setInt(3, tloustka);
            preparedStatement.setString(4, tvarderovani);
            preparedStatement.setInt(5, vyska);
            preparedStatement.setInt(6, sirka);
            preparedStatement.setString(7, material);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void removePlech(String nazev) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM Plechy WHERE nazev= ?");
            preparedStatement.setString(1, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Plechy> returnPlechy() {
        Statement stmt;
        List<Plechy> listPlechy = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from plechy");
            Plechy plechy;
            while (rs.next()) {
                plechy = new Plechy(rs.getInt(3), rs.getString(2), rs.getInt(6), rs.getInt(7), rs.getInt(4), rs.getString(5), rs.getString(8));
                listPlechy.add(plechy);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listPlechy;
    }

    public void showPlechy() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Plechy");
            while (rs.next())
                System.out.println(rs.getString(2) + "  " + rs.getInt(3) + "  " + rs.getInt(4) + "  " + rs.getInt(5) + "  " + rs.getInt(6) + "  " + rs.getInt(7) + "  " + rs.getInt(8) + " " + rs.getString(9));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(String nazev, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Plechy SET kusu = ? WHERE nazev= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(int id, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Plechy SET kusu = ? WHERE id= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
