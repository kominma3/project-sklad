package cz.filtry.database;

import cz.filtry.model.Krabice;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class KrabiceDatabase {
    private Connection connection;

    public KrabiceDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addKrabici(String nazev, int kusu, int vyska, int sirka, int delka, String vrstvy) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("insert into Krabice (nazev,kusu,vyska,sirka,delka,vrstvy) values(?,?,?,?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setInt(2, kusu);
            preparedStatement.setInt(3, vyska);
            preparedStatement.setInt(4, sirka);
            preparedStatement.setInt(5, delka);
            preparedStatement.setString(6, vrstvy);


            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Krabice> filterKrabice() {
        return null;
    }

    public void removeKrabici(String nazev) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM Krabice WHERE nazev= ?");
            preparedStatement.setString(1, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void showKrabice() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Krabice");
            while (rs.next())
                System.out.println(rs.getString(2) + "  " + rs.getInt(3) + "  " + rs.getInt(4) + "  " + rs.getInt(5) + "  " + rs.getInt(6) + "  " + rs.getString(7));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Krabice> returnkrabice() {
        Statement stmt;
        List<Krabice> listKrabice = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from krabice");
            Krabice krabice;
            while (rs.next()) {
                krabice = new Krabice(rs.getInt(3), rs.getString(2), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7));
                listKrabice.add(krabice);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listKrabice;
    }

    public void changePocetKusu(String nazev, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Krabice SET kusu = ? WHERE nazev= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(int id, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Krabice SET kusu = ? WHERE id= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
