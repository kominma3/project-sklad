package cz.filtry.database;

import cz.filtry.model.Tesneni;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TesneniDatabase {
    private Connection connection;

    public TesneniDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addTesneni(String nazev, int kusu, int vnejsiprumer, int vnitrniprumer, int tloustka, String material, String typ) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("INSERT into tesneni (nazev,kusu,vnejsiprumer,vnitrniprumer,tloustka,material,typ) values(?,?,?,?,?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setInt(2, kusu);
            preparedStatement.setInt(3, vnejsiprumer);
            preparedStatement.setInt(4, vnitrniprumer);
            preparedStatement.setInt(5, tloustka);
            preparedStatement.setString(6, material);
            preparedStatement.setString(7, typ);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(String nazev, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Tesneni SET kusu = ? WHERE nazev= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Tesneni> returnTesneni() {
        Statement stmt;
        List<Tesneni> listTesneni = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from tesneni");
            Tesneni tesneni;
            while (rs.next()) {
                tesneni = new Tesneni(rs.getInt(3), rs.getString(2), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getString(8));
                listTesneni.add(tesneni);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listTesneni;
    }

    public void removeTesneni(String nazev) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM Tesneni WHERE nazev= ?");
            preparedStatement.setString(1, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
