package cz.filtry.database;

import java.sql.*;

public class OsobaDatabase {
    private Connection connection;

    public OsobaDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addOsoba(String name, String password, String salt, int right) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("insert into Osoba (name,hash,salt,prava) values(?,?,?,?)");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, salt);
            preparedStatement.setInt(4, right);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public void removeOsoba(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Osoba WHERE name = ?");
            statement.setString(1, name);
            statement.execute();
            System.out.println("done");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void showOsoby() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Osoba");
            while (rs.next())
                System.out.println(rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + "  " + rs.getInt(5));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public String getHash(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT hash FROM Osoba WHERE name = ?");
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public String getSalt(String name) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT salt FROM Osoba WHERE name = ?");
            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public boolean userExists(String name) {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select name from Osoba");
            while (rs.next())
                if (rs.getString(1).equals(name)) {
                    return true;
                }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
