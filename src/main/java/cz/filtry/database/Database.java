package cz.filtry.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class Database {
    private Connection connection;

    public Database() {
    }

    public Connection getConnection() {
        return connection;
    }

    public boolean startDatabase() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/skladtest", "root", "root");
            System.out.println("OH database");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void endDatabase() {
        try {
            connection.close();
            System.out.println("ended");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
