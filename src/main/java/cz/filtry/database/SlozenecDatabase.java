package cz.filtry.database;

import cz.filtry.model.Slozenec;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SlozenecDatabase {
    private Connection connection;

    public SlozenecDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addSlozenec(String nazev, int kusu, int vyska, int sirka, int tloustka, int pocetskladu, String material) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("INSERT into Slozenec (nazev,kusu,vyska,sirka,tloustka,pocetskladu,material) values(?,?,?,?,?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setInt(2, kusu);
            preparedStatement.setInt(3, vyska);
            preparedStatement.setInt(4, sirka);
            preparedStatement.setInt(5, tloustka);
            preparedStatement.setInt(6, pocetskladu);
            preparedStatement.setString(7, material);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Slozenec> returnSlozenec() {
        Statement stmt;
        List<Slozenec> listSlozenec = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from slozenec");
            Slozenec slozenec;
            while (rs.next()) {
                slozenec = new Slozenec(rs.getInt(3), rs.getString(2), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getString(8));
                listSlozenec.add(slozenec);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listSlozenec;
    }

    public void removeViko(String nazev) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM Slozenec WHERE nazev= ?");
            preparedStatement.setString(1, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void showSlozenec() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Slozenec");
            while (rs.next())
                System.out.println(rs.getString(2) + "  " + rs.getInt(3) + "  " + rs.getInt(4) + "  " + rs.getInt(5) + "  " + rs.getInt(6) + "  " + rs.getInt(7) + " " + rs.getString(8));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(String nazev, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Slozenec SET kusu = ? WHERE nazev= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(int id, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Slozenec SET kusu = ? WHERE id= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
