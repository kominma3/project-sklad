package cz.filtry.database;

import cz.filtry.model.Vika;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VikaDatabase {
    private Connection connection;

    public VikaDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addViko(String nazev, int kusu, int vnitrniprumer, int vnejsiprumer, boolean dira, String tvar, boolean pruchodnost) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("insert into Vika (nazev,kusu,vnitrni_prumer,vnejsi_prumer,dira,tvar,pruchodnost) values(?,?,?,?,?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setInt(2, kusu);
            preparedStatement.setInt(3, vnitrniprumer);
            preparedStatement.setInt(4, vnejsiprumer);
            preparedStatement.setBoolean(5, dira);
            preparedStatement.setString(6, tvar);
            preparedStatement.setBoolean(7, pruchodnost);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void removeViko(String nazev) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM Vika WHERE nazev= ?");
            preparedStatement.setString(1, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void showVika() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Vika");
            while (rs.next())
                System.out.println(rs.getString(2) + "  " + rs.getInt(3) + "  " + rs.getInt(4) + "  " + rs.getInt(5) + "  " + rs.getBoolean(6) + "  " + rs.getString(7) + "  " + rs.getBoolean(8));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Vika> returnVika() {
        Statement stmt;
        List<Vika> vika = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Vika");
            Vika viko;
            while (rs.next()) {
                viko = new Vika(rs.getInt(3), rs.getString(2), rs.getInt(4), rs.getInt(5), rs.getBoolean(6), rs.getString(7), rs.getBoolean(8));
                //System.out.println(rs.getString(2) + "  " + rs.getInt(3) + "  " + rs.getInt(4) + "  " + rs.getInt(5) + "  " + rs.getBoolean(6) + "  " + rs.getString(7) + "  " + rs.getBoolean(8));
                vika.add(viko);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return vika;
    }

    public void changePocetKusu(String nazev, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Vika SET kusu = ? WHERE nazev= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(int id, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Vika SET kusu = ? WHERE id= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
