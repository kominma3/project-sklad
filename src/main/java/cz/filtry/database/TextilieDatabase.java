package cz.filtry.database;

import cz.filtry.model.Textilie;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TextilieDatabase {
    private Connection connection;

    public TextilieDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addTextilie(String nazev, int kusu, int vyska, int delka, String material) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("INSERT into Textilie (nazev,kusu,vyska,delka,material) values(?,?,?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setInt(2, kusu);
            preparedStatement.setInt(3, vyska);
            preparedStatement.setInt(4, delka);
            preparedStatement.setString(5, material);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void removeTextilie(String nazev) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM Textilie WHERE nazev= ?");
            preparedStatement.setString(1, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void showTextilie() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Textilie");
            while (rs.next())
                System.out.println(rs.getString(2) + "  " + rs.getInt(3) + "  " + rs.getInt(4) + "  " + rs.getInt(5) + "  " + rs.getString(6));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changePocetKusu(String nazev, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Textilie SET kusu = ? WHERE nazev= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Textilie> returnTextilie() {
        Statement stmt;
        List<Textilie> listTextilie = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from textilie");
            Textilie textilie1;
            while (rs.next()) {
                textilie1 = new Textilie(rs.getInt(3), rs.getString(2), rs.getInt(4), rs.getInt(5), rs.getString(6));
                listTextilie.add(textilie1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listTextilie;
    }

    public void changePocetKusu(int id, int pocetkusu) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Textilie SET kusu = ? WHERE id= ?");
            preparedStatement.setInt(1, pocetkusu);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
