package cz.filtry.database;

import cz.filtry.model.Lepidla;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LepidlaDatabase {
    private Connection connection;

    public LepidlaDatabase(Connection connection) {
        this.connection = connection;
    }

    public void addLepidlo(String nazev, float mnozstvi, String pomer) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("insert into Lepidla (nazev,mnozstvi,misicipomer) values(?,?,?)");
            preparedStatement.setString(1, nazev);
            preparedStatement.setFloat(2, mnozstvi);
            preparedStatement.setString(3, pomer);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public void removeLepidlo(String nazev) {
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Lepidla WHERE nazev = ?");
            statement.setString(1, nazev);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Lepidla> returnLepidla() {
        Statement stmt;
        List<Lepidla> listLepidla = new ArrayList<>();
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from lepidla");
            Lepidla lepidla;
            while (rs.next()) {
                lepidla = new Lepidla(rs.getInt(3), rs.getString(2), rs.getString(4));
                listLepidla.add(lepidla);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listLepidla;
    }

    public void showLepidla() {
        Statement stmt;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Lepidla");
            while (rs.next())
                System.out.println(rs.getString(2) + " " + rs.getInt(3) + "  " + rs.getString(4));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changeMnozstvi(String nazev, float mnozstvi) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Lepidla SET mnozstvi = ? WHERE nazev= ?");
            preparedStatement.setFloat(1, mnozstvi);
            preparedStatement.setString(2, nazev);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void changeMnozstvi(int id, int mnozstvi) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement("UPDATE Lepidla SET mnozstvi = ? WHERE id= ?");
            preparedStatement.setInt(1, mnozstvi);
            preparedStatement.setInt(2, id);
            preparedStatement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
