package cz.filtry.model;

public class Textilie {
    private final String name;
    private final int vyska;
    private final int delka;
    private int kusu;
    private String material;

    public Textilie(int kusu, String name, int vyska, int delka, String material) {
        this.kusu = kusu;
        this.name = name;
        this.vyska = vyska;
        this.delka = delka;
        this.material = material;
    }

    public int getKusu() {
        return kusu;
    }

    public void setKusu(int kusu) {
        this.kusu = kusu;
    }

    public String getName() {
        return name;
    }

    public int getVyska() {
        return vyska;
    }

    public int getDelka() {
        return delka;
    }

    public String getMaterial() {
        return material;
    }
}
