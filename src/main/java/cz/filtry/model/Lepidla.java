package cz.filtry.model;

public class Lepidla {
    private final String nazev;
    private final String misiciPomer;
    private float mnozstvi;

    public Lepidla(float mnostvi, String nazev, String misici) {
        this.nazev = nazev;
        this.misiciPomer = misici;
        this.mnozstvi = mnostvi;
    }

    public String getNazev() {
        return nazev;
    }

    public String getMisiciPomer() {
        return misiciPomer;
    }

    public float getMnozstvi() {
        return mnozstvi;
    }

    public void setMnozstvi(float mnostvi) {
        this.mnozstvi = mnostvi;
    }
}
