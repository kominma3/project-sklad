package cz.filtry.model;

public class Slozenec {
    private final String name;
    private final int vyska;
    private final int sirka;
    private final int tloustka;
    private final int pocetSkladu;
    private final String material;
    private int kusu;


    public Slozenec(int kusu, String name, int vyska, int sirka, int tloustka, int pocetSkladu, String material) {
        this.name = name;
        this.kusu = kusu;
        this.vyska = vyska;
        this.sirka = sirka;
        this.tloustka = tloustka;
        this.pocetSkladu = pocetSkladu;
        this.material = material;
    }

    @Override
    public String toString() {
        return "Slozenec{" +
                "name='" + name + '\'' +
                ", kusu=" + kusu +
                ", vyska=" + vyska +
                ", sirka=" + sirka +
                ", tloustka=" + tloustka +
                ", pocetSkladu=" + pocetSkladu +
                ", material='" + material + '\'' +
                '}';
    }

    public int getVyska() {
        return vyska;
    }

    public int getSirka() {
        return sirka;
    }

    public int getTloustka() {
        return tloustka;
    }

    public int getPocetSkladu() {
        return pocetSkladu;
    }

    public String getMaterial() {
        return material;
    }

    public String getName() {
        return name;
    }

    public int getKusu() {
        return kusu;
    }

    public void setKusu(int kusu) {
        this.kusu = kusu;
    }
}
