package cz.filtry.model;

public class Tesneni {
    private final String name;
    private final int vnejsiprumer;
    private final int vnitrniprumer;
    private final int tloustka;
    private final String material;
    private final String typ;
    private int kusu;

    public Tesneni(int kusu, String name, int vnejsiprumer, int vnitrniprumer, int tloustka, String material, String typ) {
        this.name = name;
        this.kusu = kusu;
        this.vnejsiprumer = vnejsiprumer;
        this.vnitrniprumer = vnitrniprumer;
        this.tloustka = tloustka;
        this.material = material;
        this.typ = typ;
    }

    public String getName() {
        return name;
    }

    public int getKusu() {
        return kusu;
    }

    public void setKusu(int kusu) {
        this.kusu = kusu;
    }

    public int getVnejsiprumer() {
        return vnejsiprumer;
    }

    public int getVnitrniprumer() {
        return vnitrniprumer;
    }

    public int getTloustka() {
        return tloustka;
    }

    public String getMaterial() {
        return material;
    }

    public String getTyp() {
        return typ;
    }
}
