package cz.filtry.model;

public class Vika {

    private final String name;
    private final int vnitrniprumer;
    private final int vnejsiprumer;
    private final boolean dira;
    private final String tvar;
    private final boolean pruchodnost;
    private int kusu;

    public Vika(int kusu, String name, int vnitrniprumer, int vnejsiprumer, boolean dira, String tvar, boolean pruchodnost) {
        this.kusu = kusu;
        this.name = name;
        this.vnitrniprumer = vnitrniprumer;
        this.vnejsiprumer = vnejsiprumer;
        this.dira = dira;
        this.tvar = tvar;
        this.pruchodnost = pruchodnost;
    }

    @Override
    public String
    toString() {
        return "Vika{" +
                "kusu=" + kusu +
                ", name='" + name + '\'' +
                ", vnitrniprumer=" + vnitrniprumer +
                ", vnejsiprumer=" + vnejsiprumer +
                ", dira=" + dira +
                ", tvar='" + tvar + '\'' +
                ", pruchodnost=" + pruchodnost +
                '}';
    }

    public int getKusu() {
        return kusu;
    }

    public void setKusu(int kusu) {
        this.kusu = kusu;
    }

    public String getName() {
        return name;
    }

    public int getVnitrniprumer() {
        return vnitrniprumer;
    }

    public int getVnejsiprumer() {
        return vnejsiprumer;
    }

    public boolean isDira() {
        return dira;
    }

    public boolean isPruchodnost() {
        return pruchodnost;
    }

    public String getTvar() {
        return tvar;
    }
}
