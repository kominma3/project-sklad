package cz.filtry.model;

public class Plechy {
    private final String name;
    private final int vyska;
    private final int sirka;
    private final int tloustka;
    private final String tvarDerovani;
    private final String material;
    private int kusu;

    public Plechy(int kusu, String name, int vyska, int sirka, int tloustka, String tvarDerovani, String material) {
        this.name = name;
        this.kusu = kusu;
        this.vyska = vyska;
        this.sirka = sirka;
        this.tloustka = tloustka;
        this.tvarDerovani = tvarDerovani;
        this.material = material;
    }

    public String getName() {
        return name;
    }

    public int getKusu() {
        return kusu;
    }

    public void setKusu(int kusu) {
        this.kusu = kusu;
    }

    public int getVyska() {
        return vyska;
    }

    public int getSirka() {
        return sirka;
    }

    public int getTloustka() {
        return tloustka;
    }

    public String getTvarDerovani() {
        return tvarDerovani;
    }

    public String getMaterial() {
        return material;
    }

}
