package cz.filtry.model;

public class Krabice {
    private final String name;
    private final int vyska;
    private final int sirka;
    private final int delka;
    private final String vrstvy;
    private int kusu;


    public Krabice(int kusu, String name, int vyska, int sirka, int delka, String vrstvy) {
        this.name = name;
        this.kusu = kusu;
        this.vyska = vyska;
        this.sirka = sirka;
        this.delka = delka;
        this.vrstvy = vrstvy;
    }

    public String getName() {
        return name;
    }

    public int getKusu() {
        return kusu;
    }

    public int getVyska() {
        return vyska;
    }

    public int getSirka() {
        return sirka;
    }

    public int getDelka() {
        return delka;
    }

    public String getVrstvy() {
        return vrstvy;
    }
}
