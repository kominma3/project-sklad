package cz.filtry.controller.filterControllers;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.ButtonCells.ButtonCellKrabice;
import cz.filtry.controller.ButtonCells.ButtonCellVika;
import cz.filtry.controller.MainController;
import cz.filtry.database.VikaDatabase;
import cz.filtry.model.Vika;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

import java.util.List;

public class VikaController {
    public Button filtrovatButton;
    public List<Vika> vika;
    private MainController mainController;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void handleFiltrovatButton(ActionEvent actionEvent) {
        VikaDatabase vikaDatabase = new VikaDatabase(App.database.getConnection());
        vika = vikaDatabase.returnVika();
        ObservableList<Vika> list = FXCollections.observableArrayList(vika);
        //step 1 finished
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        mainController.setVikaList(vika);
        mainController.tableView.getColumns().clear();
        mainController.tableView.setEditable(true);
        TableColumn name = new TableColumn("Název");
        name.setCellValueFactory(new PropertyValueFactory<Vika, String>("name"));
        TableColumn vnitrniPrumer = new TableColumn("Vnitřní průměr");
        vnitrniPrumer.setCellValueFactory(new PropertyValueFactory<Vika, Integer>("vnitrniprumer"));
        TableColumn vnejsiPrumer = new TableColumn("Vnější Průmer");
        vnejsiPrumer.setCellValueFactory(new PropertyValueFactory<Vika, Integer>("vnejsiprumer"));
        TableColumn diraAha = new TableColumn("Víko s dírou");
        diraAha.setCellValueFactory(new PropertyValueFactory<Vika, Boolean>("dira"));
        TableColumn tvar = new TableColumn("Tvar ");
        tvar.setCellValueFactory(new PropertyValueFactory<Vika, String>("tvar"));
        TableColumn pruchod = new TableColumn("Průchozí");
        pruchod.setCellValueFactory(new PropertyValueFactory<Vika, String>("pruchodnost"));
        TableColumn pocetKusu = new TableColumn("Počet kusů");
        pocetKusu.setCellValueFactory(new PropertyValueFactory<Vika, Integer>("kusu"));
        pocetKusu.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        pocetKusu.setOnEditCommit((EventHandler<TableColumn.CellEditEvent>) t ->
        {

            String identityName = ((Vika) t.getTableView().getItems().get(t.getTablePosition().getRow())).getName();
            vikaDatabase.changePocetKusu(identityName, (Integer) t.getNewValue());

        });
        TableColumn col_action = new TableColumn<>("Action");

        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>,
                        ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                        return new SimpleBooleanProperty(p.getValue() != null);
                    }
                });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

                    @Override
                    public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                        return new ButtonCellVika(list);
                    }

                });
        mainController.tableView.getColumns().addAll(name, vnitrniPrumer, vnejsiPrumer, diraAha, tvar, pruchod, pocetKusu, col_action);
        mainController.tableView.setItems(list);
        //System.out.println(list);

    }
}
