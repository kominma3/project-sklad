package cz.filtry.controller.filterControllers;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.ButtonCells.ButtonCellKrabice;
import cz.filtry.controller.ButtonCells.ButtonCellTesneni;
import cz.filtry.controller.MainController;
import cz.filtry.database.TesneniDatabase;
import cz.filtry.model.Tesneni;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

import java.util.List;

public class TesneniController {
    public Button filtrovatButton;
    public List<Tesneni> tesneni;
    private MainController mainController;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void handleFiltrovatButton(ActionEvent actionEvent) {
        TesneniDatabase tesneniDatabase = new TesneniDatabase(App.database.getConnection());
        //tesneniDatabase.addTesneni("Test tesneni",20, 10,6,5,"zelezo","oble");
        tesneni = tesneniDatabase.returnTesneni();

        ObservableList<Tesneni> list = FXCollections.observableArrayList(tesneni);
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        mainController.setTesneniList(tesneni);
        mainController.tableView.getColumns().clear();
        mainController.tableView.setEditable(true);
        TableColumn name = new TableColumn("Název");
        name.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("name"));
        TableColumn vnejsiprumer = new TableColumn("Vnější průmer");
        vnejsiprumer.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("vnejsiprumer"));
        TableColumn vnitrniprumer = new TableColumn("Vnitřní průmer");
        vnitrniprumer.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("vnitrniprumer"));
        TableColumn tloustka = new TableColumn("Tloušťka");
        tloustka.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("tloustka"));

        TableColumn material = new TableColumn("Materiál");
        material.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("material"));
        TableColumn typ = new TableColumn("Typ");
        typ.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("typ"));
        TableColumn kusu = new TableColumn("Kusů");
        kusu.setCellValueFactory(new PropertyValueFactory<Tesneni, String>("kusu"));
        kusu.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        kusu.setOnEditCommit((EventHandler<TableColumn.CellEditEvent>) t ->
        {

            String identityName = ((Tesneni) t.getTableView().getItems().get(t.getTablePosition().getRow())).getName();
            tesneniDatabase.changePocetKusu(identityName, (Integer) t.getNewValue());

        });
        TableColumn col_action = new TableColumn<>("Action");

        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>,
                        ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                        return new SimpleBooleanProperty(p.getValue() != null);
                    }
                });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

                    @Override
                    public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                        return new ButtonCellTesneni(list);
                    }

                });
        mainController.tableView.getColumns().addAll(name, vnejsiprumer, vnitrniprumer, tloustka, material, typ, kusu, col_action);
        mainController.tableView.setItems(list);
    }
}
