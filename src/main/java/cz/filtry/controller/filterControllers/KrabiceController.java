package cz.filtry.controller.filterControllers;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.ButtonCells.ButtonCellKrabice;
import cz.filtry.controller.MainController;
import cz.filtry.database.KrabiceDatabase;
import cz.filtry.model.Krabice;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

import java.util.List;

public class KrabiceController {
    public Button filtrovatButton;
    public List<Krabice> krabice;
    public CheckBox threeBox;
    public CheckBox fiveBox;
    public TextField vyskaToTextField;
    public TextField vyskaFromTextField;
    public TextField delkaToTextField;
    public TextField delkaFromTextField;
    public TextField sirkaToTextField;
    public TextField sirkaFromTextField;
    public ObservableList<Krabice> list;
    private MainController mainController;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void handleFiltrovatButton(ActionEvent actionEvent) {
        /*Integer vyskaFrom =Integer.parseInt(vyskaFromTextField.getText());
        if(!vyskaToTextField.getText().equals("")) {
            Integer vyskaTo = Integer.parseInt(vyskaToTextField.getText());
            System.out.printf("Tak vysledek %d a %d",vyskaFrom,vyskaTo);
        }
        System.out.printf("Tak vysledek %d a null",vyskaFrom);*/
        KrabiceDatabase krabiceDatabase = new KrabiceDatabase(App.database.getConnection());
        krabice = krabiceDatabase.returnkrabice();
        list = FXCollections.observableArrayList(krabice);
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        mainController.setKrabiceList(krabice);
        mainController.tableView.getColumns().clear();
        mainController.tableView.setEditable(true);
        TableColumn name = new TableColumn("Název");
        name.setCellValueFactory(new PropertyValueFactory<Krabice, String>("name"));
        TableColumn vyska = new TableColumn("Výška");
        vyska.setCellValueFactory(new PropertyValueFactory<Krabice, Integer>("vyska"));
        TableColumn sirka = new TableColumn("Šířka");
        sirka.setCellValueFactory(new PropertyValueFactory<Krabice, String>("sirka"));
        TableColumn delka = new TableColumn("Délka");
        delka.setCellValueFactory(new PropertyValueFactory<Krabice, String>("delka"));
        TableColumn tloustka = new TableColumn("Tloušťka");
        tloustka.setCellValueFactory(new PropertyValueFactory<Krabice, String>("vrstvy"));
        TableColumn kusu = new TableColumn("Kusů");
        kusu.setCellValueFactory(new PropertyValueFactory<Krabice, String>("kusu"));
        kusu.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        kusu.setOnEditCommit((EventHandler<TableColumn.CellEditEvent>) t ->
        {

            String identityName = ((Krabice) t.getTableView().getItems().get(t.getTablePosition().getRow())).getName();
            krabiceDatabase.changePocetKusu(identityName, (Integer) t.getNewValue());

        });
        //Insert Button
        TableColumn col_action = new TableColumn<>("Action");

        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>,
                        ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                        return new SimpleBooleanProperty(p.getValue() != null);
                    }
                });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

                    @Override
                    public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                        return new ButtonCellKrabice(list);
                    }

                });

        mainController.tableView.getColumns().addAll(name, vyska, sirka, delka, tloustka, kusu, col_action);
        mainController.tableView.setItems(list);
    }

}
