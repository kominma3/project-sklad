package cz.filtry.controller.filterControllers;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.ButtonCells.ButtonCellKrabice;
import cz.filtry.controller.ButtonCells.ButtonCellSlozenec;
import cz.filtry.controller.MainController;
import cz.filtry.database.SlozenecDatabase;
import cz.filtry.model.Slozenec;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

import java.util.List;

public class SlozenecController {
    public Button filtrovatButton;
    public List<Slozenec> slozence;
    private MainController mainController;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void handleFiltrovatButton(ActionEvent actionEvent) {
        SlozenecDatabase slozenecDatabase = new SlozenecDatabase(App.database.getConnection());
        //slozenecDatabase.addSlozec("test slozenec",40, 50 ,60 , 4,2,"filtr A");
        slozence = slozenecDatabase.returnSlozenec();
        ObservableList<Slozenec> list = FXCollections.observableArrayList(slozence);
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        mainController.setSlozenecList(slozence);
        mainController.tableView.getColumns().clear();
        mainController.tableView.setEditable(true);
        TableColumn name = new TableColumn("Název");
        name.setCellValueFactory(new PropertyValueFactory<Slozenec, String>("name"));
        TableColumn vyska = new TableColumn("Výška");
        vyska.setCellValueFactory(new PropertyValueFactory<Slozenec, Integer>("vyska"));
        TableColumn sirka = new TableColumn("Šířka");
        sirka.setCellValueFactory(new PropertyValueFactory<Slozenec, String>("sirka"));
        TableColumn tloustka = new TableColumn("Tloušťka");
        tloustka.setCellValueFactory(new PropertyValueFactory<Slozenec, String>("tloustka"));

        TableColumn material = new TableColumn("Materiál");
        material.setCellValueFactory(new PropertyValueFactory<Slozenec, String>("material"));
        TableColumn pocetskladu = new TableColumn("Pocet Skladu");
        pocetskladu.setCellValueFactory(new PropertyValueFactory<Slozenec, String>("pocetSkladu"));
        TableColumn kusu = new TableColumn("Kusů");
        kusu.setCellValueFactory(new PropertyValueFactory<Slozenec, String>("kusu"));
        kusu.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        kusu.setOnEditCommit((EventHandler<TableColumn.CellEditEvent>) t ->
        {

            String identityName = ((Slozenec) t.getTableView().getItems().get(t.getTablePosition().getRow())).getName();
            slozenecDatabase.changePocetKusu(identityName, (Integer) t.getNewValue());

        });
        TableColumn col_action = new TableColumn<>("Action");

        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>,
                        ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                        return new SimpleBooleanProperty(p.getValue() != null);
                    }
                });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

                    @Override
                    public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                        return new ButtonCellSlozenec(list);
                    }

                });
        mainController.tableView.getColumns().addAll(name, vyska, sirka, tloustka, pocetskladu, material, kusu, col_action);
        mainController.tableView.setItems(list);
    }
}
