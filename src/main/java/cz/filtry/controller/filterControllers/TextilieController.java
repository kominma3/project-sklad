package cz.filtry.controller.filterControllers;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.ButtonCells.ButtonCellKrabice;
import cz.filtry.controller.ButtonCells.ButtonCellTextilie;
import cz.filtry.controller.MainController;
import cz.filtry.database.TextilieDatabase;
import cz.filtry.model.Textilie;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

import java.util.List;

public class TextilieController {
    public Button filtrovatButton;
    public List<Textilie> textilie;
    private MainController mainController;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void handleFiltrovatButton(ActionEvent actionEvent) {

        TextilieDatabase textilieDatabase = new TextilieDatabase(App.database.getConnection());
        textilie = textilieDatabase.returnTextilie();
        //textilieDatabase.addTextilie("Test textilie", 20, 200, 100, "hedvabi");
        ObservableList<Textilie> list = FXCollections.observableArrayList(textilie);
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        mainController.setTextilieList(textilie);
        mainController.tableView.getColumns().clear();
        mainController.tableView.setEditable(true);
        TableColumn name = new TableColumn("Název");
        name.setCellValueFactory(new PropertyValueFactory<Textilie, String>("name"));
        TableColumn vyska = new TableColumn("Výška");
        vyska.setCellValueFactory(new PropertyValueFactory<Textilie, String>("vyska"));
        TableColumn delka = new TableColumn("Šířka");
        delka.setCellValueFactory(new PropertyValueFactory<Textilie, String>("delka"));
        TableColumn material = new TableColumn("Materiál");
        material.setCellValueFactory(new PropertyValueFactory<Textilie, String>("material"));
        TableColumn kusu = new TableColumn("Kusů");
        kusu.setCellValueFactory(new PropertyValueFactory<Textilie, String>("kusu"));
        kusu.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        kusu.setOnEditCommit((EventHandler<TableColumn.CellEditEvent>) t ->
        {

            String identityName = ((Textilie) t.getTableView().getItems().get(t.getTablePosition().getRow())).getName();
            textilieDatabase.changePocetKusu(identityName, (Integer) t.getNewValue());

        });
        TableColumn col_action = new TableColumn<>("Action");

        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>,
                        ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                        return new SimpleBooleanProperty(p.getValue() != null);
                    }
                });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

                    @Override
                    public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                        return new ButtonCellTextilie(list);
                    }

                });
        mainController.tableView.getColumns().addAll(name, vyska, delka, material, kusu, col_action);
        mainController.tableView.setItems(list);
    }

}
