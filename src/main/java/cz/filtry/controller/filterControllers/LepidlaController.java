package cz.filtry.controller.filterControllers;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.ButtonCells.ButtonCellLepidla;
import cz.filtry.controller.MainController;
import cz.filtry.database.LepidlaDatabase;
import cz.filtry.model.Lepidla;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.FloatStringConverter;

import java.util.List;

public class LepidlaController {
    public Button filtrovatButton;
    public List<Lepidla> lepidla;
    private MainController mainController;

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void handleFiltrovatButton(ActionEvent actionEvent) {
        LepidlaDatabase lepidlaDatabase = new LepidlaDatabase(App.database.getConnection());
        //lepidlaDatabase.addLepidlo("test lepidlo",3.5f,"2:1");
        lepidla = lepidlaDatabase.returnLepidla();
        ObservableList<Lepidla> list = FXCollections.observableArrayList(lepidla);
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        mainController.setLepidlaList(lepidla);
        mainController.tableView.getColumns().clear();
        mainController.tableView.setEditable(true);
        TableColumn name = new TableColumn("Název");
        name.setCellValueFactory(new PropertyValueFactory<Lepidla, String>("nazev"));
        TableColumn misiciPomer = new TableColumn("Mísící poměr");
        misiciPomer.setCellValueFactory(new PropertyValueFactory<Lepidla, String>("misiciPomer"));
        TableColumn mnozstvi = new TableColumn("Množství");
        mnozstvi.setCellValueFactory(new PropertyValueFactory<Lepidla, Float>("mnozstvi"));
        mnozstvi.setCellFactory(TextFieldTableCell.forTableColumn(new FloatStringConverter()));
        mnozstvi.setOnEditCommit((EventHandler<TableColumn.CellEditEvent>) t ->
        {

            String identityName = ((Lepidla) t.getTableView().getItems().get(t.getTablePosition().getRow())).getNazev();
            lepidlaDatabase.changeMnozstvi(identityName, (Float) t.getNewValue());

        });
        TableColumn col_action = new TableColumn<>("Action");

        col_action.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<Disposer.Record, Boolean>,
                        ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Disposer.Record, Boolean> p) {
                        return new SimpleBooleanProperty(p.getValue() != null);
                    }
                });

        //Adding the Button to the cell
        col_action.setCellFactory(
                new Callback<TableColumn<Disposer.Record, Boolean>, TableCell<Disposer.Record, Boolean>>() {

                    @Override
                    public TableCell<Disposer.Record, Boolean> call(TableColumn<Disposer.Record, Boolean> p) {
                        return new ButtonCellLepidla(list);
                    }

                });
        mainController.tableView.getColumns().addAll(name, misiciPomer, mnozstvi, col_action);
        mainController.tableView.setItems(list);
    }
}
