package cz.filtry.controller;

import cz.filtry.controller.filterControllers.*;
import cz.filtry.model.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MainController {
    public Button VikaButton;
    public Button SlozenecButton;
    public Button KrabiceButton;
    public Button PlechyButton;
    public Button LepidlaButton;
    public Button TesneniButton;
    public Button TextilieButton;
    public Button addButton;
    public MenuItem logoutMenu;
    public TableView tableView;
    private List<Vika> vikaList;
    private List<Textilie> textilieList;
    private List<Tesneni> tesneniList;
    private List<Slozenec> slozenecList;
    private List<Plechy> plechyList;
    private List<Krabice> krabiceList;
    private List<Lepidla> lepidlaList;

    public List<Vika> getVikaList() {
        return vikaList;
    }

    public void setVikaList(List<Vika> vikaList) {
        this.vikaList = vikaList;
    }

    public void setLepidlaList(List<Lepidla> lepidlaList) {
        this.lepidlaList = lepidlaList;
    }

    public void setKrabiceList(List<Krabice> krabice) {
        this.krabiceList = krabice;
    }

    public void setPlechyList(List<Plechy> plechy) {
        this.plechyList = plechy;
    }

    public void setSlozenecList(List<Slozenec> slozenecList) {
        this.slozenecList = slozenecList;
    }

    public void setTextilieList(List<Textilie> textilieList) {
        this.textilieList = textilieList;
    }

    public void setTesneniList(List<Tesneni> tesneniList) {
        this.tesneniList = tesneniList;
    }


    public void hadleVikaButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/vikafilter.fxml"));
            Parent root = fxmlLoader.load();
            VikaController vikaController = fxmlLoader.getController();
            vikaController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("Vika filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleAddButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/windows/addNewItem.fxml"));
            Parent root = fxmlLoader.load();
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("Add new Item");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void hadleSlozenecButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/slozenecfilter.fxml"));
            Parent root = fxmlLoader.load();
            SlozenecController slozenecController = fxmlLoader.getController();
            slozenecController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("SLozenec filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hadleKrabiceButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/krabicefilter.fxml"));
            Parent root = fxmlLoader.load();
            KrabiceController krabiceController = fxmlLoader.getController();
            krabiceController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("krabice filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hadlePlechyButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/plechyfilter.fxml"));
            Parent root = fxmlLoader.load();
            PlechyController plechyController = fxmlLoader.getController();
            plechyController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("Plechy filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hadleLepidlaButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/lepidlafilter.fxml"));
            Parent root = fxmlLoader.load();
            LepidlaController lepidlaController = fxmlLoader.getController();
            lepidlaController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("Lepidla filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hadleTesneniButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/Tesnenifilter.fxml"));
            Parent root = fxmlLoader.load();
            TesneniController tesneniController = fxmlLoader.getController();
            tesneniController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("Tesneni filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hadleTexitileButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/filters/textiliefilter.fxml"));
            Parent root = fxmlLoader.load();
            TextilieController textilieController = fxmlLoader.getController();
            textilieController.setMainController(this);
            Stage window = new Stage();
            window.initModality(Modality.APPLICATION_MODAL);
            window.setTitle("Textilie filter");
            Scene scene = new Scene(root);
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlelogoutMenu() {
        try {
            Path current = Paths.get("").toAbsolutePath();
            Parent root = FXMLLoader.load(getClass().getResource("/view/windows/login.fxml"));
            Stage stage = (Stage) VikaButton.getScene().getWindow();
            Scene scene = new Scene(root);
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
