package cz.filtry.controller;

import cz.filtry.App;
import cz.filtry.PasswordHash;
import cz.filtry.database.OsobaDatabase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LoginController {
    public Button login;
    public Button button;
    public TextField username;
    public PasswordField password;
    public Label wrongInput;
    public Button backButton;

    public void hadleButtonClick() {
        button.setText("zmena");
    }

    @FXML
    public void hadleLoginButton() {
        String name = username.getText();
        String heslo = password.getText();
        OsobaDatabase osobaDatabase = new OsobaDatabase(App.database.getConnection());
        System.out.println();
        if (osobaDatabase.userExists(name)) {
            if (PasswordHash.verifyPassword(heslo, osobaDatabase.getHash(name), osobaDatabase.getSalt(name))) {
                try {
                    Path current = Paths.get("").toAbsolutePath();
                    System.out.println(current);
                    MainController controller = new MainController();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main.fxml"));
                    loader.setController(controller);
                    Parent root = loader.load();
                    Stage stage = (Stage) button.getScene().getWindow();
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    //stage.show();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                wrongInput.setText("Spatny heslo");
                wrongInput.setVisible(true);
            }
        } else {
            wrongInput.setText("Uživatelské jméno neexistuje");
            wrongInput.setVisible(true);
        }
    }

    public void handleAddButton(ActionEvent actionEvent) {
    }

    public void handleBackButton(ActionEvent actionEvent) {
    }
}
