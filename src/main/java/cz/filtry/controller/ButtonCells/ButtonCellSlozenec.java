package cz.filtry.controller.ButtonCells;


import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.SureDeleteController;
import cz.filtry.database.SlozenecDatabase;
import cz.filtry.model.Slozenec;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;


public class ButtonCellSlozenec extends TableCell<Disposer.Record, Boolean> {
    final Button cellButton = new Button("Delete");

    public ButtonCellSlozenec(ObservableList<?> list) {

        //Action when the button is pressed
        cellButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                boolean answer = SureDeleteController.display();
                if (answer) {
                    Slozenec slozenec = (Slozenec) ButtonCellSlozenec.this.getTableView().getItems().get(ButtonCellSlozenec.this.getIndex());
                    SlozenecDatabase slozenecDatabase = new SlozenecDatabase(App.database.getConnection());
                    slozenecDatabase.removeViko(slozenec.getName());
                    list.remove(slozenec);
                }
                // get Selected Item


                //remove selected item from the table list
                //data.remove(currentPerson);
            }
        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(Boolean t, boolean empty) {
        super.updateItem(t, empty);
        if (!empty) {
            setGraphic(cellButton);
        } else {
            setGraphic(null);
        }
    }
}
