package cz.filtry.controller.ButtonCells;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.SureDeleteController;
import cz.filtry.database.LepidlaDatabase;
import cz.filtry.model.Lepidla;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;


public class ButtonCellLepidla extends TableCell<Disposer.Record, Boolean> {
    final Button cellButton = new Button("Delete");
    private final ButtonCellLepidla selfReference = this;

    public ButtonCellLepidla(ObservableList<?> list) {

        //Action when the button is pressed
        cellButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                boolean answer = SureDeleteController.display();
                if (answer) {
                    Lepidla currentLepidla = (Lepidla) ButtonCellLepidla.this.getTableView().getItems().get(ButtonCellLepidla.this.getIndex());
                    LepidlaDatabase lepidlaDatabase = new LepidlaDatabase(App.database.getConnection());
                    lepidlaDatabase.removeLepidlo(currentLepidla.getNazev());
                    list.remove(currentLepidla);
                }
                // get Selected Item


                //remove selected item from the table list
                //data.remove(currentPerson);
            }
        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(Boolean t, boolean empty) {
        super.updateItem(t, empty);
        if (!empty) {
            setGraphic(cellButton);
        } else {
            setGraphic(null);
        }
    }
}