package cz.filtry.controller.ButtonCells;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.SureDeleteController;
import cz.filtry.database.KrabiceDatabase;
import cz.filtry.model.Krabice;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;


public class ButtonCellKrabice extends TableCell<Disposer.Record, Boolean> {
    final Button cellButton = new Button("Delete");

    public ButtonCellKrabice(ObservableList<?> list) {

        //Action when the button is pressed
        cellButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                boolean deleteConfirmed = SureDeleteController.display();
                if (deleteConfirmed) {
                    Krabice currentKrabice = (Krabice) ButtonCellKrabice.this.getTableView().getItems().get(ButtonCellKrabice.this.getIndex());
                    KrabiceDatabase krabiceDatabase = new KrabiceDatabase(App.database.getConnection());
                    krabiceDatabase.removeKrabici(currentKrabice.getName());
                    list.remove(currentKrabice);
                }
                // get Selected Item


                //remove selected item from the table list
                //data.remove(currentPerson);
            }
        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(Boolean t, boolean empty) {
        super.updateItem(t, empty);
        if (!empty) {
            setGraphic(cellButton);
        } else {
            setGraphic(null);
        }
    }
}

