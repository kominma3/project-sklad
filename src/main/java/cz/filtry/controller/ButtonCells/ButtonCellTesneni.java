package cz.filtry.controller.ButtonCells;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.SureDeleteController;
import cz.filtry.database.TesneniDatabase;
import cz.filtry.model.Tesneni;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;


public class ButtonCellTesneni extends TableCell<Disposer.Record, Boolean> {
    final Button cellButton = new Button("Delete");

    public ButtonCellTesneni(ObservableList<?> list) {

        //Action when the button is pressed
        cellButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                boolean answer = SureDeleteController.display();
                if (answer) {
                    Tesneni currentTesneni = (Tesneni) ButtonCellTesneni.this.getTableView().getItems().get(ButtonCellTesneni.this.getIndex());
                    TesneniDatabase tesneniDatabase = new TesneniDatabase(App.database.getConnection());
                    tesneniDatabase.removeTesneni(currentTesneni.getName());
                    list.remove(currentTesneni);
                }
                // get Selected Item


                //remove selected item from the table list
                //data.remove(currentPerson);
            }
        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(Boolean t, boolean empty) {
        super.updateItem(t, empty);
        if (!empty) {
            setGraphic(cellButton);
        } else {
            setGraphic(null);
        }
    }
}
