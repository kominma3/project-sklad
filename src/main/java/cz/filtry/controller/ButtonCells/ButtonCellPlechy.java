package cz.filtry.controller.ButtonCells;

import com.sun.prism.impl.Disposer;
import cz.filtry.App;
import cz.filtry.controller.SureDeleteController;
import cz.filtry.database.PlechyDatabase;
import cz.filtry.model.Plechy;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;


public class ButtonCellPlechy extends TableCell<Disposer.Record, Boolean> {
    final Button cellButton = new Button("Delete");

    public ButtonCellPlechy(ObservableList<?> list) {

        //Action when the button is pressed
        cellButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {

                boolean answer = SureDeleteController.display();
                if (answer) {
                    Plechy currentPlech = (Plechy) ButtonCellPlechy.this.getTableView().getItems().get(ButtonCellPlechy.this.getIndex());
                    PlechyDatabase plechyDatabase = new PlechyDatabase(App.database.getConnection());
                    plechyDatabase.removePlech(currentPlech.getName());
                    list.remove(currentPlech);
                }
                // get Selected Item


                //remove selected item from the table list
                //data.remove(currentPerson);
            }
        });
    }

    //Display button if the row is not empty
    @Override
    protected void updateItem(Boolean t, boolean empty) {
        super.updateItem(t, empty);
        if (!empty) {
            setGraphic(cellButton);
        } else {
            setGraphic(null);
        }
    }
}
