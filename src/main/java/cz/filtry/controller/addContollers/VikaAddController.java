package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.VikaDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class VikaAddController {
    public Button addButton;
    public Button backButton;
    public ChoiceBox materialBox;
    public TextField diraButton;
    public TextField vnejsiButton;
    public TextField vnitrniButton;
    public TextField nameText;
    public TextField kusuButton;
    public ToggleGroup group;

    public void handleAddButton(ActionEvent actionEvent) {
        Stage stage = (Stage) addButton.getScene().getWindow();
        VikaDatabase vikaDatabase = new VikaDatabase(App.database.getConnection());
        String name = nameText.getText();
        int kusu = Integer.parseInt(kusuButton.getText());
        int vnejsi = Integer.parseInt(vnejsiButton.getText());
        int vnitrni = Integer.parseInt(vnitrniButton.getText());
        String material = (String) materialBox.getSelectionModel().getSelectedItem();
        RadioButton tvar = (RadioButton) group.getSelectedToggle();
        String tvarName = tvar.getText();
        vikaDatabase.addViko(name, kusu, vnitrni, vnejsi, true, tvarName, true);
        //add to database
        stage.close();
    }

    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }
}
