package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.TesneniDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class TesneniAddController {
    public TextField vnejsiPrumerTextBox;
    public TextField vnitrniPrumerTextBox;
    public TextField tloustkaTextBox;
    public ChoiceBox materialChoiceBox;
    public Button addNewButton;
    public Button backButton;
    public TextField nameTextField;
    public TextField pocetKusuTextField;
    public ToggleGroup group;

    public void handleAddNewButton(ActionEvent actionEvent) {
        TesneniDatabase tesneniDatabase = new TesneniDatabase(App.database.getConnection());
        String name = nameTextField.getText();
        int kusu = Integer.parseInt(pocetKusuTextField.getText());
        int vnejsiPrumer = Integer.parseInt(vnejsiPrumerTextBox.getText());
        int vnitrniPrumer = Integer.parseInt(vnitrniPrumerTextBox.getText());
        int tloustka = Integer.parseInt(tloustkaTextBox.getText());
        String material = (String) materialChoiceBox.getSelectionModel().getSelectedItem();
        RadioButton button = (RadioButton) group.getSelectedToggle();
        String type = button.getText();
        tesneniDatabase.addTesneni(name, kusu, vnejsiPrumer, vnitrniPrumer, tloustka, material, type);
        Stage stage = (Stage) addNewButton.getScene().getWindow();
        stage.close();

    }

    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }
}
