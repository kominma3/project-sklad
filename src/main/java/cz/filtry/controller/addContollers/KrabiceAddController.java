package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.KrabiceDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

public class KrabiceAddController {
    public TextField sirkaTextField;
    public TextField vyskaTextField;
    public TextField delkaTextField;
    public ToggleGroup materialGroup;
    public Button addButton;
    public Button backButton;
    public TextField nameTextField;
    public TextField kusuTextField;

    public void handleAddButton(ActionEvent actionEvent) {
        KrabiceDatabase krabiceDatabase = new KrabiceDatabase(App.database.getConnection());
        String name = nameTextField.getText();
        int vyska = Integer.parseInt(vyskaTextField.getText());
        int sirka = Integer.parseInt(sirkaTextField.getText());
        int delka = Integer.parseInt(delkaTextField.getText());
        int kusu = Integer.parseInt(kusuTextField.getText());
        RadioButton radioButton = (RadioButton) materialGroup.getSelectedToggle();
        String vrstvy = radioButton.getText();
        krabiceDatabase.addKrabici(name, kusu, vyska, sirka, delka, vrstvy);
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }

    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }
}
