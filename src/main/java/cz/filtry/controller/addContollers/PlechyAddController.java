package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.PlechyDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class PlechyAddController {
    public Button backButton;
    public Button addButton;
    public TextField delkaTextField;
    public TextField sirkaTextField;
    public TextField nameTextField;
    public TextField kusuTextField;
    public ToggleGroup typGroup;
    public ChoiceBox materialChoiceBox;
    public TextField tloustkaTextField;

    public void handleAddButton(ActionEvent actionEvent) {
        PlechyDatabase plechyDatabase = new PlechyDatabase(App.database.getConnection());
        String name = nameTextField.getText();
        int kusu = Integer.parseInt(kusuTextField.getText());
        int tloustka = Integer.parseInt(tloustkaTextField.getText());
        RadioButton typRadio = (RadioButton) typGroup.getSelectedToggle();
        String typ = typRadio.getText();
        int vyska = Integer.parseInt(delkaTextField.getText());
        int sirka = Integer.parseInt(sirkaTextField.getText());
        String material = (String) materialChoiceBox.getSelectionModel().getSelectedItem();
        plechyDatabase.addPlech(name, kusu, tloustka, typ, vyska, sirka, material);
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }

    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }
}
