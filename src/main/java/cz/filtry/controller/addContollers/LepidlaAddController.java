package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.LepidlaDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

public class LepidlaAddController {

    public ToggleGroup typGroup;
    public TextField nameTextFireld;
    public TextField misiciPomerTextField;
    public Button addButton;
    public Button backButton;
    public TextField kusuTextField;

    public void handleAddButton(ActionEvent actionEvent) {
        LepidlaDatabase lepidlaDatabase = new LepidlaDatabase(App.database.getConnection());
        String name = nameTextFireld.getText();
        float mnozstvi = Float.parseFloat(kusuTextField.getText());
        String pomer = misiciPomerTextField.getText();
        lepidlaDatabase.addLepidlo(name, mnozstvi, pomer);
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }

    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }
}
