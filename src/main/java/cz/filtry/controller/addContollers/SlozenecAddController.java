package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.SlozenecDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

public class SlozenecAddController {
    public TextField vyskaSkladuTextField;
    public TextField sirkaSkladuTextField;
    public TextField pocetSkalduTextField;
    public ChoiceBox materialChoiceBox;
    public ToggleGroup group;
    public TextField teplotaTextField;
    public TextField sirkaRoleTextField;
    public TextField hmotnostRoleTextField;
    public Button addButton;
    public Button backButton;
    public TextField nameTextField;
    public TextField kusuTextField;

    public void handleAddButton(ActionEvent actionEvent) {
        SlozenecDatabase slozenecDatabase = new SlozenecDatabase(App.database.getConnection());
        String name = nameTextField.getText();
        int kusu = Integer.parseInt(kusuTextField.getText());
        int vyska = Integer.parseInt(vyskaSkladuTextField.getText());
        int sirka = Integer.parseInt(sirkaSkladuTextField.getText());
        int tloustka = Integer.parseInt(sirkaRoleTextField.getText()); // TODO: 04.07.2021  zjistit spravne parametry
        int pocetSkladu = Integer.parseInt(pocetSkalduTextField.getText());
        String material = (String) materialChoiceBox.getSelectionModel().getSelectedItem();


        slozenecDatabase.addSlozenec(name, kusu, vyska, sirka, tloustka, pocetSkladu, material);
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }

    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }
}
