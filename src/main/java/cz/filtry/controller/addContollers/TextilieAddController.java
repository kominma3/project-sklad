package cz.filtry.controller.addContollers;

import cz.filtry.App;
import cz.filtry.database.TextilieDatabase;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TextilieAddController {
    public ChoiceBox materialChoiceBox;
    public TextField vyskaTextField;
    public TextField delkaTextField;
    public TextField nameTextField;
    public TextField pocetKusuTextField;
    public Button backButton;
    public Button addButton;


    public void handleBackButton(ActionEvent actionEvent) {
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }

    public void handleAddButton(ActionEvent actionEvent) {
        TextilieDatabase textilieDatabase = new TextilieDatabase(App.database.getConnection());
        int vyska = Integer.parseInt(vyskaTextField.getText());
        int delka = Integer.parseInt(delkaTextField.getText());
        String name = nameTextField.getText();
        int kusu = Integer.parseInt(pocetKusuTextField.getText());
        String material = (String) materialChoiceBox.getSelectionModel().getSelectedItem();
        textilieDatabase.addTextilie(name, kusu, vyska, delka, material);
        Stage stage = (Stage) addButton.getScene().getWindow();
        stage.close();
    }
}
