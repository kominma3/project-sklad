package cz.filtry.controller;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class SureDeleteController {
    private static boolean answer = false;


    public static boolean display() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("potvrdit vymazani");
        window.setMinWidth(250);
        Label label = new Label();
        label.setText("Opravdu chcete smazat tuto položku");
        Button yesButton = new Button("Ano");
        Button noButton = new Button("Ne");

        yesButton.setOnAction(event -> {

            answer = true;
            window.close();
        });
        noButton.setOnAction(event -> {
            answer = false;
            window.close();
        });
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, yesButton, noButton);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        return answer;


    }
}
