package cz.filtry;

import cz.filtry.database.Database;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Hello world!
 */


public class App extends Application {
    public static Database database;

    public static void main(String[] args) {
        database = new Database();
        database.startDatabase();
        launch(args);
        database.endDatabase();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        System.out.println(getClass().getResource("./"));
        Parent root = FXMLLoader.load(getClass().getResource("/view/windows/login.fxml"));
        primaryStage.setTitle("Filtry");
        primaryStage.setScene(new Scene(root, 900, 500));
        primaryStage.show();
    }


}
